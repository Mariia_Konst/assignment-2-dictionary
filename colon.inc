%define my_dict 0
%macro colon 2
    %ifid %2
        %ifstr %1
            %2:
                dq my_dict
                db %1, 0
                %define my_dict %2
        %else
            %error "Неправильный ключ"
        %endif
    %else
        %error "Неправильное значение"
    %endif
%endmacro
