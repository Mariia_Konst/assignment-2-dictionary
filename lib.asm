global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global read_string
global parse_uint
global parse_int
global string_copy
global print_error


section .data
    char_buffer db 0  ; Буфер для считанного символа
    tab db 0x9     ; Табуляция
    newline db 0xA ; Перевод строки
    null_term db 0  ; Нуль-терминатор
    minus_sign db '-'   ; Символ минуса
    space   db ' ', '\t', '\n'  ; Пробельные символы


section .bss
    buffer resb 256  ; Буфер для хранения слова, здесь размер буфера 256 байт



section .text
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov  rax, 60
    xor  rdi, rdi
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
        xor rcx, rcx      ; Инициализируем счетчик (длину) в нуль
    .loop:
        mov al, byte [rdi + rcx]  ; Загружаем символ из строки
        cmp al, 0                 ; Сравниваем его с нулевым символом
        je .done                  ; Если это нуль-терминатор, завершаем цикл
        inc rcx                   ; Иначе увеличиваем счетчик
        jmp .loop                 ; Переходим к следующему символу
    .done:
        mov rax, rcx
        ret


; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi         ; Сохраняем регистр rdi, чтобы не потерять его
    call string_length  ; Вызываем функцию string_length для определения длины строки
    pop  rsi         ; Восстанавливаем rsi из стека, теперь rsi указывает на строку
    mov  rdx, rax    ; Устанавливаем длину строки в rdx
    mov  rax, 1      ; Номер системного вызова для write
    mov  rdi, 1      ; Файловый дескриптор для stdout
    syscall           ; Вызываем системный вызов для вывода строки
    ret              ; Возвращаем управление

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    ; Установить аргументы для системного вызова write
    mov rax, 1         ; Номер системного вызова для write
    mov rdi, 1         ; Файловый дескриптор для stdout
    mov rsi, rsp  ; Указатель на символ
    mov rdx, 1         ; Длина строки (1 символ)

    ; Вызвать системный вызов write
    syscall

    pop rdi

    ret
    
    
; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    ; Передать код символа новой строки (0xA) в регистре RDI
    mov rsp, 0xA
    call print_char ; Вызвать функцию print_char для вывода новой строки
    ret


; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.

        
; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi      ; Помещаем входное число (rdi) в регистр rax
    xor rdx, rdx      ; Обнуляем rdx для деления
    mov rcx, rsp      ; Устанавливаем rcx в вершину стека (буфер для ASCII-символов)
    dec rsp           ; Уменьшаем стек, чтобы освободить место для символов
    mov byte[rsp],dl  ; Помещаем ноль (dl) в верхний байт стека (нижний байт уже содержит 0)
    mov r9, 10        ; Загружаем значение 10 (для деления) в регистр r9
    
    .digitdiv: 
        xor rdx, rdx        ; Обнуляем rdx для деления
        div r9              ; Делим rax на 10, результат в rax, остаток в rdx
        add rdx, '0'        ; Преобразуем остаток в ASCII и сохраняем в стеке
        dec rsp             ; Смещаем указатель стека
        mov byte [rsp], dl  ; Записываем ASCII-символ в стеке
        test rax, rax       ; Проверяем, закончено ли деление
        jnz .digitdiv


    mov rdi, rsp         ; Устанавливаем rdi на начало строки (вершину стека)
    push rcx             ; Сохраняем rcx
    call print_string    ; Выводим строку
    pop rsp              ; Восстанавливаем стек
    ret


; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0           ; Сравниваем rdi (входное число) с нулем
    jge .positive        ; Если больше или равно 0, переходим к выводу положительного числа

    push rdi             ; Сохраняем rdi на стеке
    mov rdi, '-'         ; Устанавливаем '-' в rdi (для вывода знака минуса)
    call print_char      ; Вызываем функцию print_char для вывода минуса
    pop rdi              ; Восстанавливаем rdi с оригинальным значением
    neg rdi              ; Инвертируем rdi, чтобы получить положительное значение

.positive:
    call print_uint      ; Вызываем функцию print_uint для вывода положительного числа
    ret



; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals: 
    xor rcx, rcx 
    mov rax, 1 
    .compare: 
        mov al, byte [rdi] 
        cmp al, byte [rsi] 
        jne .notEqual 
        test al, al 
        je .equal 
        inc rdi 
        inc rsi 
        jmp .compare 
    .equal: 
        mov rax, 1 
        ret 
    .notEqual: 
        xor rax, rax 
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    ; Установить аргументы для системного вызова read
    mov rax, 0           ; Номер системного вызова read (sys_read)
    mov rdi, 0           ; Файловый дескриптор stdin (0)
    mov rsi, char_buffer ; Указатель на буфер для считанного символа
    mov rdx, 1           ; Количество байт для чтения (1 символ)

    syscall

    ; Проверить, было ли считано что-либо
    cmp rax, 0
    jle .end_of_stream

    ; Вернуть считанный символ
    mov al, [char_buffer]
    ret

    .end_of_stream:
        ; Конец потока, вернуть 0
        xor rax, rax
        ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    xor rax, rax              ; Очищаем rax, чтобы сохранить результат
    mov r10, rdi             ; Сохраняем rdi (указатель на начало строки) в r10

.skip_spaces:
    push rdi                 ; Сохраняем регистры на стеке
    push rsi
    push r10
    call read_char           ; Вызываем функцию read_char для чтения символа
    pop r10
    pop rsi
    pop rdi


    cmp rax, 0xA             
    jz .skip_spaces          
    cmp rax, 0x9             
    jz .skip_spaces          
    cmp rax, 0x20            
    jz .skip_spaces          

.read:
    test rax, rax            ; Проверяем, является ли символ пробельным
    jz .end            ; Если да, завершаем слово
    cmp rax, 0xA
    jz .end
    cmp rax, 0x9
    jz .end
    cmp rax, 0x20
    jz .end


    dec rsi                  ; Уменьшаем счетчик символов
    test rsi, rsi            ; Проверяем, остались ли еще символы
    jz .err                  ; Если нет, переходим к .err

    mov byte [rdi], al      ; Сохраняем символ в памяти
    inc rdi                  ; Увеличиваем указатель в памяти

    push rdi                 ; Сохраняем регистры на стеке
    push rsi
    push r10
    call read_char           ; Читаем следующий символ
    pop r10
    pop rsi
    pop rdi
    jmp .read                ; Повторяем процесс для следующего символа

.end:
    mov byte [rdi], 0        ; Устанавливаем нулевой терминатор для завершения строки
    mov rax, r10             ; Восстанавливаем rax с началом слова
    sub rdi, r10             ; Вычисляем длину слова
    mov rdx, rdi             ; Сохраняем длину слова в rdx
    ret

.err:
    xor rdx, rdx
    xor rax, rax             ; Возвращаем 0 в rax в случае ошибки
    ret





; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx: его длину в символах
; rdx = 0 если число прочитать не удалось
; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    mov rdx, 0      ; Инициализация длины числа в 0
    xor rax, rax    ; Обнуление результата

    ; Парсинг числа
    .parse_number:
        movzx ecx, byte [rdi]   ; Загрузка символа из строки в ecx
        cmp cl, 0                   ; Проверка на нулевой символ (конец строки)
        je .done

        ; Проверить, является ли символ цифрой (0-9)
        cmp cl, '0'
        jb .done  ; Если символ меньше '0', завершить
        cmp cl, '9'
        ja .done  ; Если символ больше '9', завершить

        ; Преобразовать ASCII-цифру в число и добавить к результату
        sub cl, '0'
        imul rax, rax, 10  ; Умножить текущее число на 10
        add rax, rcx       ; Добавить новую цифру к результату

        inc rdi             ; Перейти к следующему символу
        inc rdx             ; Увеличить длину числа на 1
        jmp .parse_number
    .done:
        ret



; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte [rdi], '-'  ; Сравниваем первый байт строки с знаком минус
    jne .parse_uint      ; Если не равен "-", переходим к обработке положительного числа

    inc rdi             ; Пропускаем символ "-"
    call parse_uint     ; Вызываем функцию для парсинга беззнакового числа
    neg rax             ; Инвертируем результат для получения отрицательного числа
    inc rdx             ; Увеличиваем длину числа (с учетом знака)
    ret

.parse_uint:
    call parse_uint     ; Вызываем функцию для парсинга положительного числа
    ret 



; Принимает указатель на строку (rdi), указатель на буфер (rsi) и длину буфера (rdx)
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax          ; Обнуляем rax, чтобы хранить длину строки
.copy_loop:
    mov al, [rdi]         ; Загружаем байт из строки в al
    test al, al           ; Проверяем, является ли это концом строки (нулевым символом)
    jz .end_copy          ; Если да, завершаем копирование
    dec rdx               ; Уменьшаем длину буфера
    jz .buffer_full       ; буфер полон
    mov [rsi], al         ; Копируем байт из строки в буфер
    inc rsi               ; Увеличиваем указатель на буфер
    inc rdi               ; Увеличиваем указатель на строку
    inc rax               ; Увеличиваем длину строки
    jmp .copy_loop        ; Повторяем процесс
.buffer_full:
    xor rax, rax          ; Если буфер полон, обнуляем rax
.end_copy:
    mov byte [rsi], 0     ; Устанавливаем нулевой символ в конец буфера
    ret

print_error:
    mov rsi, rdi
    push rsi
    call string_length
    mov rdx, rax
    mov rax, 1
    mov rdi, 2
    pop rsi
    syscall
    ret



read_string:
    push r12
    push r13
    push r14
    mov r12, rdi            ; r12 - адрес буфера
    mov r13, rsi            ; r13 - размер буфера
    dec r13                 ; в буфере нужно место под терминатора
    xor r14, r14            ; r14 - текущая длина слова
    .loop:
        call read_char
        test rax, rax       ; EOF?
        je .success
        cmp r14, r13
        jge .too_long
        mov [r12+r14], rax
        inc r14
        jmp .loop
    .success:
        mov byte[r12+r14+1], 0
        jmp .end
    .too_long:
        xor r12, r12
    .end:
        mov rax, r12
        mov rdx, r14
    pop r14
    pop r13
    pop r12
    ret
