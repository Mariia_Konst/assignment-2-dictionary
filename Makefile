ASM = nasm
ASMFLAGS = -felf64
PYTHON = python

%.o: %.asm
	$(ASM) $(ASMFLAGS) $< -o $@

program: main.o lib.o dict.o
	ld -o $@ $^

.PHONY: run test clean

clean:
	rm *.o

run:
	./program
	
test:
	$(PYTHON) test.py
