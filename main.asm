%include "lib.inc"
%include "words.inc"
%include "dict.inc"

%define SYS_STDIN 0
%define SYS_STDOUT 1
%define SYS_STDERR 2
%define BUF_SIZE 255
%define PTR_SIZE 8

section .bss
    buffer: resb BUF_SIZE

section .rodata
    ERR_MSG_INPUT: db "Input error", 0
    ERR_MSG_NOT_FOUND: db "Слово не найдено", 0

section .text
global _start

_start:
    ; Чтение строки из stdin в буфер
    mov rdi, buffer
    mov rsi, BUF_SIZE
    call read_string

    ; Проверка на корректное считывание слова из stdin
    test rax, rax
    jz .error_input

    ; Поиск слова в словаре
    push rdx
    mov rdi, rax
    mov rsi, my_dict  ;  имя словаря
    call find_word

    ; Проверка на наличие слова в словаре
    test rax, rax
    jz .error_not_found

    ; Вывод значения в stdout
    pop rdx
    lea rdi, [rax+8+rdx+1]          ; получаем адрес значения, сместившись через указатель на предыдущую запись (+8) и строку ключа (rdx+1)
    call print_string


    ; Завершение программы
    mov rdi, 0
    call exit

.error_input:
    ; Вывод сообщения об ошибке считывания в stderr
    mov rdi, ERR_MSG_INPUT
    jmp .err

.error_not_found:
    ; Вывод сообщения об отсутствии слова в stderr
    mov rdi, ERR_MSG_NOT_FOUND
    jmp .err

.err:
    mov rsi, SYS_STDERR
    call print_string

    ; Завершение программы с кодом ошибки
    mov rdi, 1
    call exit