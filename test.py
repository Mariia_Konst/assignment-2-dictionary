import subprocess

def run_test(input_data, expected_output):
    command = ["./program"]
    try:
        subproc = subprocess.Popen(command, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout, stderr = subproc.communicate(input=input_data.encode(), timeout=5)
        out = stdout.decode().strip()
        assert out == expected_output, f"Expected: {expected_output}, Got: {out}"
    except subprocess.TimeoutExpired:
        assert False, "Test execution timed out"

def test_main():
    test_cases = [
        {"input": "Соль", "expected_output": "s"},
        {"input": "b", "expected_output": "b"},
        {"input": "", "expected_output": "a"},
    ]

    for i, test_case in enumerate(test_cases, start=1):
        input_data = test_case["input"]
        expected_output = test_case["expected_output"]
        try:
            run_test(input_data, expected_output)
            print(f"Test {i} passed.")
        except AssertionError as e:
            print(f"Test {i} failed: {e}")

if __name__ == "__main__":
    test_main()